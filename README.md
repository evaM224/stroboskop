# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://evaM224@bitbucket.org/evaM224/stroboskop.git


```

Naloga 6.2.3:
https://bitbucket.org/evaM224/stroboskop/commits/b06097ec0bb5122ce33ce00db12045a9d5cacad1

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/evaM224/stroboskop/commits/4720f0507279dcb8b747405c74894d5d72dda7cc

Naloga 6.3.2:
https://bitbucket.org/evaM224/stroboskop/commits/06cb0d575f9052527090b39c82157b838e7d8b81

Naloga 6.3.3:
https://bitbucket.org/evaM224/stroboskop/commits/5ae45377b3c8c405244411ceb1227b462de4cf38

Naloga 6.3.4:
https://bitbucket.org/evaM224/stroboskop/commits/a727bdc0c993f36b83f95e6fca758f6de67c4ee7

Naloga 6.3.5:

```
git checkout master
git merge izgled

```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/evaM224/stroboskop/commits/f3b90e70f7ce931dc7f8dbb7c83382cb36383fff

Naloga 6.4.2:
https://bitbucket.org/evaM224/stroboskop/commits/d72b8b8c76b77c26dce7c4d36c5d069222bc379d

Naloga 6.4.3:
https://bitbucket.org/evaM224/stroboskop/commits/3e892e17b65168ff91539c15a139a72c1c77440e

Naloga 6.4.4:
https://bitbucket.org/evaM224/stroboskop/commits/e3c438ec442576b35aa5c96fbbef1ab502ffd8a0